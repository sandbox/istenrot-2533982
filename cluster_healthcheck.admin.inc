<?php

/**
 * @file
 * Administrative functionality for healthcheck page module.
 */

/**
 * Container for the callback management interface.
 *
 * @return string
 *   Rreturns rendered HTML.
 */
function cluster_healthcheck_admin_overview() {
  $output = theme('fieldset', array(
    'element' => array(
      '#title' => t('Manage'),
      '#children' => theme('cluster_healthcheck_callbacks_manage_render'),
    ),
  ));

  return $output;
}

/**
 * Theme function to render the callback management admin interface.
 *
 * @return string
 *   Returns rendered HTML.
 */
function theme_cluster_healthcheck_callbacks_manage_render() {
  global $language;
  $callbacks = cluster_healthcheck_get_callbacks();

  $edit_access = user_access('administer site configuration');

  if (!empty($language->prefix)) {
    $prefix = '/' . $language->prefix . '/';
  }
  else {
    $prefix = '/';
  }
  $rows = array();
  foreach ($callbacks as $cid => $callback) {
    $rows[$cid][] = '<strong>' . $prefix . $callback->path . '</strong>';
    $rows[$cid][] = l(t('Hosts'), 'admin/structure/cluster-healthcheck/' . $callback->cid . '/hosts');;
    $rows[$cid][] = l(t('View'), $callback->path);
    if ($edit_access) {
      $rows[$cid][] = l(t('Edit'), 'admin/structure/cluster-healthcheck/' . $callback->cid . '/edit');
      $rows[$cid][] = l(t('Delete'), 'admin/structure/cluster-healthcheck/' . $callback->cid . '/delete');
    }
  }

  $header = array();
  if ($edit_access) {
    $header = array(
      t('URL'),
      t('View registered hosts'),
      t('View page'),
      t('Edit'),
      t('Delete'),
    );
  }

  $admin_add = t('<a href="@url">Add one now!</a>', array('@url' => url('admin/structure/cluster-healthcheck/add')));

  if (count($callbacks)) {
    $output = theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    $output = '<p style="padding:10px 0;margin:0;">' . t('No callbacks exist yet.') . ($edit_access ? ' ' . $admin_add : '') . '</p>';
  }

  return $output;
}

/**
 * Container for the callback management interface.
 *
 * @param int $cid
 *   ID number of a callback.
 *
 * @return string
 *   Returns rendered HTML.
 */
function cluster_healthcheck_admin_hosts($cid = NULL) {
  global $language;
  if (empty($cid)) {
    drupal_goto('admin/structure/cluster-healthcheck');
  }
  $cid = intval($cid);
  $path = '(path not set)';
  $result = cluster_healthcheck_get_callback($cid);
  if ($result) {
    $path = $result->path;
  }
  if (!empty($language->prefix)) {
    $prefix = '/' . $language->prefix . '/';
  }
  else {
    $prefix = '/';
  }
  $output = theme(
    'fieldset',
    array(
      'element' => array(
        '#title' => t('Manage hosts for %path', array('%path' => $prefix . $path)),
        '#children' => theme('cluster_healthcheck_hosts_render'),
      ),
    )
  );

  return $output;
}

/**
 * Theme function to render the callback management admin interface.
 *
 * @return string
 *   Returns rendered HTML.
 */
function theme_cluster_healthcheck_hosts_render() {
  $cid = intval(arg(3));
  $hosts = cluster_healthcheck_get_hosts($cid);
  if (!is_array($hosts)) {
    $hosts = array();
  }

  $edit_access = user_access('administer site configuration');

  $rows = array();
  foreach ($hosts as $id => $host) {
    switch ($host->status) {
      case 0:
        $host_status = t('DOWN');
        break;

      case 1:
        $host_status = t('UP');
        break;

      case 2:
        $host_status = '<i>' . t('%status (pending for next health check)', array('%status' => 'DOWN')) . '</i>';
        break;

      case 3:
        $host_status = '<i>' . t('%status (pending for next health check)', array('%status' => 'UP')) . '</i>';
        break;

      default:
        $host_status = 'Unknown';
    }
    $rows[$id][] = '<strong>' . $host->host . '</strong>';
    $rows[$id][] = $host_status;
    if ($edit_access) {
      switch ($host->status) {
        case 0:
        case 2:
          $rows[$id][] = l(t('Set up'), 'admin/structure/cluster-healthcheck/' . $cid . '/hosts/' . $host->id . '/up');
          $rows[$id][] = '';
          break;

        case 1:
        case 3:
          $rows[$id][] = '';
          $rows[$id][] = l(t('Set down'), 'admin/structure/cluster-healthcheck/' . $cid . '/hosts/' . $host->id . '/down');
          break;

        default:
          $rows[$id][] = '';
          $rows[$id][] = '';
      }
    }
  }

  $header = array();
  if ($edit_access) {
    $header = array(
      t('Host'),
      t('Status'),
      t('Action set up'),
      t('Action set down'),
    );
  }

  if (count($hosts)) {
    $output = theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    $output = '<p style="padding:10px 0;margin:0;">' . t('No hosts registered yet.') . '</p>';
  }

  return $output;
}

/**
 * The healthcheck page callback delete confirmation form constructor.
 *
 * @param array $form
 *   Array of form elements.
 * @param array $form_state
 *   Array containing information of current state of the form.
 * @param int $cid
 *   ID number of a callback requested to be deleted derived from URI path.
 *
 * @return array
 *   Array of form elements.
 */
function cluster_healthcheck_admin_delete_form(array $form, array &$form_state, $cid) {
  // Get current editor properties.
  $callback = cluster_healthcheck_get_callback($cid);
  if (!$callback) {
    drupal_goto('admin/structure/cluster-healthcheck');
  }

  drupal_set_title('Delete callback');

  $form = confirm_form(
    array(
      'callback' => array(
        '#type' => 'value',
        '#value' => $callback,
      ),
    ),
    t('Are you sure you want to delete the callback for <em>!path</em>?', array('!path' => $callback->path)),
    'admin/structure/cluster-healthcheck',
    t('This action cannot be undone.'),
    t('Delete callback'),
    t('Cancel')
  );

  $form_state['#redirect'] = 'admin/structure/cluster-healthcheck';

  return $form;
}

/**
 * Form submit handler for deleting an healthcheck page callback.
 *
 * @param array $form
 *   Array of form elements.
 * @param array $form_state
 *   Array of form state.
 */
function cluster_healthcheck_admin_delete_form_submit(array $form, array &$form_state) {
  cluster_healthcheck_delete_callback($form_state['values']['callback']->cid);

  // Clear menu cache.
  menu_rebuild();

  drupal_set_message(t('Callback <em>!path</em> deleted.', array('!path' => $form_state['values']['callback']->path)));
}

/**
 * The healthcheck page callback add / edit form.
 *
 * @param array $form
 *   Array of form elements.
 * @param array $form_state
 *   Array of current statue of the form.
 * @param int $cid
 *   ID number of a callback to be edited.
 *
 * @return array
 *   Array of form elements.
 */
function cluster_healthcheck_callbacks_form(array $form, array &$form_state, $cid = NULL) {
  global $language;
  $callback = NULL;

  // If $cid exists, we're editing.
  if (isset($cid)) {
    $callback = cluster_healthcheck_get_callback($cid);
  }

  if (!empty($language->prefix)) {
    $prefix = $language->prefix . '/';
  }
  else {
    $prefix = '';
  }
  if ($callback) {
    $form['cluster_healthcheck_callback_cid'] = array(
      '#type' => 'hidden',
      '#value' => $callback->cid,
    );
    $form_title = t('Edit callback');
  }
  else {
    $form_title = t('Create a new callback');
  }
  $form['cluster_healthcheck_basic'] = array(
    '#type' => 'fieldset',
    '#title' => $form_title,
    '#description' => t('Create a new server health check page for load balancers.'),
    '#collapsible' => TRUE,
  );
  $form['cluster_healthcheck_basic']['cluster_healthcheck_callback_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Internal path (Prefix: /%prefix', array('%prefix' => $prefix)),
    '#description' => '',
    '#required' => 1,
    '#default_value' => $callback ? $callback->path : '',
  );
  $form['cluster_healthcheck_basic']['cluster_healthcheck_callback_admin_notes'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin notes'),
    '#description' => '',
    '#default_value' => $callback ? check_plain($callback->admin_notes) : '',
  );
  $form['cluster_healthcheck_basic']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => $callback ? t('Save') : t('Add'),
  );

  return $form;
}

/**
 * Form validate handler for adding / editing an healthcheck page callback.
 *
 * @param array $form
 *   Array of form elements.
 * @param array $form_state
 *   Array containing current form state.
 */
function cluster_healthcheck_callbacks_form_validate(array $form, array &$form_state) {
  if (empty($form_state['values']['cluster_healthcheck_callback_path'])) {
    form_set_error('cluster_healthcheck_callback_path', t('You must provide an <em>Internal path</em> for your callback.'));
  }
  if (($form_state['values']['cluster_healthcheck_callback_path']) != check_plain($form_state['values']['cluster_healthcheck_callback_path'])) {
    form_set_error('cluster_healthcheck_callback_path', t('Invalid characters in the path field.'));
  }
}

/**
 * Form submit handler for adding / editing an healthcheck page callback.
 *
 * @param array $form
 *   Array of form elements.
 * @param array $form_state
 *   Array containing current form state.
 */
function cluster_healthcheck_callbacks_form_submit(array $form, array &$form_state) {
  $is_existing_callback = isset($form_state['values']['cluster_healthcheck_callback_cid']);

  $callback = new stdClass();

  if ($is_existing_callback) {
    // We're editing an existing callback.
    $callback->cid = $form_state['values']['cluster_healthcheck_callback_cid'];
  }
  else {
    // This is a new editor.
    $callback->created = REQUEST_TIME;
  }

  $callback->path = $form_state['values']['cluster_healthcheck_callback_path'];
  $callback->admin_notes = $form_state['values']['cluster_healthcheck_callback_admin_notes'];
  $callback->changed = REQUEST_TIME;

  cluster_healthcheck_save_callback($callback);

  menu_rebuild();

  drupal_set_message(t('Changes saved.'));

  $redirect = 'admin/structure/cluster-healthcheck';
  drupal_goto($redirect);
}
