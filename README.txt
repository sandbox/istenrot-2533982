Cluster Health Check
====================

ACKNOWLDGEMENTS
---------------

This is a fork of Empty Page (empty_page 7.x-1.0) Drupal contrib module made by
Nick Robillard.

INTRODUCTION
------------

With this module site admins are able to control load balancers using health
check "pages" (callbacks). Each callback registers all host names that has been
used to call that single callback. Once hosts are registered to a database
administrator can set each host status either "up" or "down" from Drupal
administrator interface.

Once admin has requested a status change for a host it will be shown in
pending state as long as the load balancer has acknowledged the status change.

When a health check page is "UP" or "UP/pending" the callback will return
HTTP sttus 200. If the state is "DOWN" or "DOWN/pending" the callback will
return HTTP status 503.

A health check page will fail with HTTP status 500 if Drupal's database is
inaccessible.

There's no limit how many different health check pages a single Drupal site
can have.

REQUIREMENTS
------------

Drupal 7.

CONFIGURATION
-------------

Create new health check pages and edit existing ones in
 Structure » Health check pages

To change hosts' statuses open "Hosts" link for options. You need to have
a load balancer running on that health check URL in order to see available
hosts in the cluster. A load balancer must poll the health check pages as
an anonymous user.

HOST DISCOVERY IN A CLUSTER
---------------------------

Once a load balancer is running health check polls on a health check page,
each time the callback compares list of registered hosts to the current
return value of PHP function php_uname("n") which is equivalent to Linux/UNIX
"uname -n" command output. If the server host name is new it will be added to a
database table of reqistered hosts. A load balancer's health check poll
request's Host: header doesn't affect on outcome so it can be anything. Only
unique server host names are registered as discovered.

CREDITS
-------

Made for the Drupal community by Ilari Stenroth with support from Exove Ltd.
